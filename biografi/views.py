from django.shortcuts import render, redirect
from .models import MyTask
from .form import kocak_form



# Create your views here.

def biografi(request):
    return render(request, 'index.html')

def contact(request):
    return render(request, "contact.html")

def ability(request):
    return render(request, "ability.html")

def experience(request):
    return render(request, "experience.html")

def aboutme(request):
    return render(request, "aboutme.html")

def saran(request):
    return render(request, "saran.html")

# def kocak(request):
#     lis = Person.objects.all()
#     return render(request, "kocak.html", {'liss' : lis})

def masukin_data(request):
    form = kocak_form(request.POST or None)

    if request.method == 'POST':
        if 'id' in request.POST:
            MyTask.objects.get(id=request.POST['id']).delete()
            return redirect('/schedule/')

        if form.is_valid():
            form.save()
            
            form = kocak_form()

    lis = MyTask.objects.all().order_by('Date')
    args = {'form': form, 'liss' : lis}
    return render(request, 'kocak.html', args)