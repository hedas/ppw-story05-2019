from django.db import models
from datetime import date
from django.core.validators import MinValueValidator, MaxValueValidator

# Create your models here.
class MyTask(models.Model):

    DATE = (
        ('1', '1'), ('2', '2'), ('3', '3'), ('4', '4'), ('5', '5'),
        ('6', '6'), ('7', '7'), ('8', '8'), ('9', '9'), ('10', '10'),
        ('11', '11'), ('12', '12'), ('13', '13'), ('14', '14'), ('15', '15'),
        ('16', '16'), ('17', '17'), ('18', '18'), ('19', '19'), ('20', '20'),
        ('21', '21'), ('22', '22'), ('23', '23'), ('24', '24'), ('25', '25'),
        ('26', '26'), ('27', '27'), ('28', '28'), ('29', '29'), ('30', '30'),
        ('31', '31'),
    )

    MONTH = (
        ('Jan' , 'January'),
        ('Feb' , 'February'),
        ('Mar' , 'March'),
        ('Apr' , 'April'),
        ('May' , 'May'),
        ('June' , 'June'),
        ('July' , 'July'),
        ('Aug' , 'August'),
        ('Sep' , 'September'),
        ('Oct' , 'October'),
        ('Nov' , 'November'),
        ('Dec' , 'December'),
    )

    Activity = models.CharField(max_length=25)
    Category = models.CharField(max_length=25)
    # Date = models.CharField(max_length=2, choices=DATE, default='1')
    Place = models.CharField(max_length=40)
    Date = models.DateField()
    # Month = models.CharField(max_length=4, choices=MONTH, default='Jan')
    # Year = models.PositiveIntegerField(validators=[MaxValueValidator(9999)], default='1970')
    Time = models.TimeField()

    def __str__(self):
        return "{} {} {} {} {}".format(self.Activity, self.Place, self.Category, self.Date, self.Time)
