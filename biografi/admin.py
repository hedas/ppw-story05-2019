from django.contrib import admin

# Register your models here
from .models import MyTask

admin.site.register(MyTask)
