from django import forms

from .models import MyTask
class kocak_form(forms.ModelForm):
    # Task = forms.CharField(label='First Name ', max_length=25)
    # Course = forms.CharField(label='Last Name ', max_length=25)

    class Meta:
        model = MyTask
        fields = [
            'Activity',
            'Category',
            'Place',
            'Time',
            'Date',
            # 'Month',
            # 'Year',
        ]

        widgets = {
            'Activity' : forms.TextInput(
                attrs = {
                    'class' :  'form-control',
                    'id' : "validationServer01",
                    'placeholder' : "Activity Name",
                }
            ),
            'Category' : forms.TextInput(
                attrs = {
                    'class' :  'form-control',
                    'id' : "validationServer01",
                    'placeholder' : "Category",
                }
            ),
            'Place' : forms.TextInput(
                attrs = {
                    'class' :  'form-control',
                    'id' : "validationServer01",
                    'placeholder' : "Place",
                }
            ),
            'Date' : forms.DateInput(
                attrs = {
                    'class' :  'form-control vDateField',
                    'id' : "Date",
                }
            ),
            'Time' : forms.TimeInput(
                attrs = {
                    'class' :  'form-control vTimeField',
                    'id' : "Time",
                }
            ),
        }
